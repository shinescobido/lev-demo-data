package com.etradingsoftware.levfin.levdemodata;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.core.MongoTemplate;
import com.etradingsoftware.levfin.levdemodata.model.Axe;
import com.etradingsoftware.levfin.levdemodata.model.AxeTarget;
import com.etradingsoftware.levfin.levdemodata.model.Client;
import com.etradingsoftware.levfin.levdemodata.model.Deal;
import com.etradingsoftware.levfin.levdemodata.model.Holding;
import com.etradingsoftware.levfin.levdemodata.model.HoldingType;
import com.etradingsoftware.levfin.levdemodata.model.Trade;
import com.etradingsoftware.levfin.levdemodata.model.Trader;
import com.etradingsoftware.levfin.levdemodata.model.Tranche;
import com.etradingsoftware.levfin.levdemodata.model.TrancheHolding;

@SpringBootApplication
public class LevDemoDataApplication implements CommandLineRunner {

  @Autowired
  private YAMLConfig confFile;

  @Autowired
  private MongoTemplate dbTemplate;

  private Set<Trader> traders;
  private Set<Client> clients;
  private Set<Deal> deals;
  private Set<Axe> axes;
  private Set<Trade> trades;
  private Set<Holding> holdings;
  private int noOfDays;

  public static void main(String[] args) {
    SpringApplication.run(LevDemoDataApplication.class, args);
  }

  public void run(String... args) throws Exception {
    noOfDays = confFile.getNoOfDays();
    System.out.println("Creating " + noOfDays + " days worth of data...");

    trades = new HashSet<Trade>();
    holdings = new HashSet<Holding>();
    createTraders();
    createClients();
    createDeals();
    createAxes();

    persistData();

    List<AxeTarget> axeTargets = new ArrayList<AxeTarget>();

    // create axe targeting
    for (int i = 0; i < noOfDays; i++) {

      Axe selectedAxe = randomAxe(); // deal, tranche, side
      AxeTarget target = new AxeTarget();
      target.setClientName(selectedAxe.getClient());
      List<Trade> targetTrades = new ArrayList<Trade>();
      for (Trade trade : trades) {
        if (selectedAxe.getDeal().equals(trade.getDeal())
            && selectedAxe.getTranche().equals(trade.getTranche())
            && selectedAxe.getSide().equals(trade.getSide())) {
          targetTrades.add(trade);
        }
      }

      List<Axe> targetAxes = new ArrayList<Axe>();
      for (Axe currAxe : axes) {
        if (selectedAxe.getDeal().equals(currAxe.getDeal())
            && selectedAxe.getTranche().equals(currAxe.getTranche())) {
          targetAxes.add(currAxe);
        }
      }
    }
    System.exit(0);
  }

  private void persistData() {
    System.out.println("Persisting all...");
    //Comment out line if data is already present to avoid duplicates (trader, clients, deals)
    traders.forEach(trader -> trader = dbTemplate.save(trader));
    clients.forEach(client -> client = dbTemplate.save(client));
    deals.forEach(deal -> deal = dbTemplate.save(deal));
    axes.forEach(axe -> axe = dbTemplate.save(axe));
    trades.forEach(trade -> trade = dbTemplate.save(trade));
    holdings.forEach(holding -> holding = dbTemplate.save(holding));
  }

  private void createAxes() {
    axes = new HashSet<Axe>();

    int ctr = 0;
    LocalDate startDate = LocalDate.now().minusDays(noOfDays);
    System.out.println("Start Date: " + startDate);
    for (int i = 0; i < noOfDays; i++) {
      LocalDate currDate = startDate.plusDays(1);
      startDate = currDate;
     
      int withStatusCtr = 4;
      int indiPriceCtr = 6;
      for (int j = 0; j < 7; j++) {
        Instant instant = currDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
        long timeInMillis = instant.toEpochMilli();
        int randomNum = ThreadLocalRandom.current().nextInt(0, 3);
        Deal randDeal = randomDeal();
        Axe axe = Axe.createAxe();

        Date createDate = new Date(timeInMillis);
        
        String dealName = randDeal.getName();
        double randomPrice = randomPrice();
        String side = ctr % 2 == 0 ? "BUYER" : "SELLER";
        int size = generateRandomDigits(3) * 10000;
        Trader randomTrader = randomTrader();

        axe.setCreateDate(createDate);
        axe.setUpdateDate(createDate);
        axe.setRollDate(createDate);
        String clientName = randomClient().getName();
        axe.setClient(clientName);

        axe.setDeal(dealName);
        axe.setPrice(randomPrice);
        axe.setSize(size);

        axe.setSide(side);
        String tranche;
        if (j < 3) { // force more eur tranches
          tranche = "€ TL";
        } else {
          tranche = randDeal.getTranches().get(randomNum).getName();
        }
        axe.setTranche(tranche);
        axe.setTrader(randomTrader.getInitials());
        axe.setComments("");
        ctr++;

        if (withStatusCtr > 0) {
          String tradeStatus = withStatusCtr % 2 == 0 ? "TRADED" : "TRADED AWAY";
          axe.setTradeStatus(tradeStatus);

          // create trade here
          if (tradeStatus.equals("TRADED")) {
            Trade newTrade = new Trade(UUID.randomUUID().toString());
            newTrade.setDeal(dealName);
            newTrade.setTranche(tranche);
            newTrade.setCreateDate(createDate);
            newTrade.setUpdateDate(createDate);
            newTrade.setFees("");
            newTrade.setClient(clientName);
            newTrade.setPrice(randomPrice);
            newTrade.setSide(side);
            newTrade.setSize(size);
            newTrade.setTerms("");
            newTrade.setTrader(randomTrader.getLastName());
            newTrade.setComments("");
            trades.add(newTrade);
            createTradeHolding(newTrade);
          }
          withStatusCtr--;
        } else {
          if (indiPriceCtr > 0) {
            axe.setComments("with random comment");
            axe.setIndicativePrice(randomPrice);
            indiPriceCtr--;
          }
        }
        axes.add(axe);
        createAxeHolding(axe);
      }
    }
  }
  
  private void createAxeHolding(Axe axe) {
    Holding newHolding = new Holding();
    newHolding.setHoldingId(UUID.randomUUID().toString());
    newHolding.setClient(axe.getClient());
    newHolding.setDeal(axe.getDeal());
    newHolding.setType(HoldingType.AXE);
    newHolding.setCreateDate(new Date());
    newHolding.setUpdateDate(new Date());
    List<TrancheHolding> trancheHoldings = new ArrayList<TrancheHolding>();
    TrancheHolding tranche = new TrancheHolding();
    tranche.setTrancheId(axe.getTranche());
    tranche.setSum(randomPrice());
    trancheHoldings.add(tranche);
    newHolding.setTranches(trancheHoldings);
    
    holdings.add(newHolding);
  }
  
  private void createTradeHolding(Trade trade) {
    Holding newHolding = new Holding();
    newHolding.setHoldingId(UUID.randomUUID().toString());
    newHolding.setClient(trade.getClient());
    newHolding.setDeal(trade.getDeal());
    newHolding.setType(HoldingType.TRADE);
    newHolding.setCreateDate(new Date());
    newHolding.setUpdateDate(new Date());
    List<TrancheHolding> trancheHoldings = new ArrayList<TrancheHolding>();
    TrancheHolding tranche = new TrancheHolding();
    tranche.setTrancheId(trade.getTranche());
    tranche.setSum(randomPrice());
    trancheHoldings.add(tranche);
    newHolding.setTranches(trancheHoldings);
    holdings.add(newHolding);
  }

  private double randomPrice() {
    double randomValue = 1 + Math.random() * 190;
    double tempRes = Math.floor(randomValue * 10);
    return tempRes / 10;
  }

  private void createDeals() {
    deals = confFile.getDeals();
    deals.forEach(deal -> {
      deal.setDealId(UUID.randomUUID().toString());
      deal.setCreateDate(new Date());
      deal.setUpdateDate(new Date());
      deal.setTrader(randomTrader().getFullName());
      deal.setTranches(randomTranches());
    });

  }

  private List<Tranche> randomTranches() {
    String[] currencies = {"€", "£", "$"};
    List<Tranche> randTranches = new ArrayList<Tranche>();
    for (String curr : currencies) {
      randTranches.add(new Tranche(curr + " TL", String.valueOf(generateRandomDigits(8)),
          String.valueOf(generateRandomDigits(5))));
    }

    return randTranches;
  }

  private void createTraders() {
    traders = new HashSet<Trader>();
    Set<String> configTrader = confFile.getTraders();

    for (String trader : configTrader) {
      Trader newTrader = new Trader();

      String[] fullname = trader.split(" ");
      String firstname = fullname[0];
      String lastName = fullname[1];
      newTrader.setFirstName(firstname);
      newTrader.setLastName(lastName);
      newTrader.setEmail(firstname.concat(".").concat(lastName).concat("@etradingsoftware.com"));
      newTrader.setInitials(firstname.charAt(0) + "" + lastName.charAt(0));
      newTrader.setPhone(String.valueOf(generateRandomDigits(8)));
      traders.add(newTrader);
    }
  }

  private void createClients() {
    clients = confFile.getClients();

    for (Client client : clients) {
      client.setClientId(UUID.randomUUID().toString());
      int randTraderCnt = generateRandomDigits(1);
      List<Trader> traderSet = new ArrayList<Trader>();
      for (int i = 0; i < randTraderCnt; i++) {
        traderSet.add(randomTrader());
      }
      client.setTraders(traderSet);
    }
  }

  private Object randomizer(Set set) {
    Random rand = new Random();
    int index = rand.nextInt(set.size());
    Iterator<Object> iter = set.iterator();
    for (int i = 0; i < index; i++) {
      iter.next();
    }
    return iter.next();
  }


  private Trader randomTrader() {
    return (Trader) randomizer(traders);
  }

  private Client randomClient() {
    return (Client) randomizer(clients);
  }

  private Deal randomDeal() {
    return (Deal) randomizer(deals);
  }

  private Axe randomAxe() {
    return (Axe) randomizer(axes);
  }

  private int generateRandomDigits(int n) {
    int m = (int) Math.pow(10, n - 1);
    return m + new Random().nextInt(9 * m);
  }
}
