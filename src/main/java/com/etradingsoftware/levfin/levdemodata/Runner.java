package com.etradingsoftware.levfin.levdemodata;

import java.util.Arrays;

public class Runner {

  public static void main(String[] args) {

    int[] given = {2, 3, 4, 5, 7, 8};
    int neededSum = 12;

    System.out.println(Arrays.toString(new Runner().result(given, neededSum)));
  }

  public int[] result(int[] given, int neededSum) {
    int[] result = new int[2];
    for (int i = 0; i < given.length; i++) {
      for (int j = 0; j < given.length; j++) {
        int sum = given[i] + given[j];
        if (sum == neededSum) {
          result[0] = i;
          result[1] = j;
          return result;
        }
      }
    }

    return result;
  }

}
