package com.etradingsoftware.levfin.levdemodata.model;

import java.util.Date;
import org.bson.types.ObjectId;

public class AxeTarget {

  private String clientName;
  private String match;
  private ObjectId matchId;
  private Boolean nothingToDo;
  private Boolean notInterested;
  private Date lastUpdate;

  public String getClientName() {
    return clientName;
  }

  public void setClientName(String clientName) {
    this.clientName = clientName;
  }

  public String getMatch() {
    return match;
  }

  public void setMatch(String match) {
    this.match = match;
  }

  public ObjectId getMatchId() {
    return matchId;
  }

  public void setMatchId(ObjectId matchId) {
    this.matchId = matchId;
  }

  public Boolean getNothingToDo() {
    return nothingToDo;
  }

  public void setNothingToDo(Boolean nothingToDo) {
    this.nothingToDo = nothingToDo;
  }

  public Boolean getNotInterested() {
    return notInterested;
  }

  public void setNotInterested(Boolean notInterested) {
    this.notInterested = notInterested;
  }

  public Date getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(Date lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

}
