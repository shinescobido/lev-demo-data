package com.etradingsoftware.levfin.levdemodata.model;

import java.util.Date;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "holding")
public class Holding {

  @Id
  private ObjectId id;

  private String holdingId;
  private String client;
  private String deal;
  private HoldingType type;
  private List<TrancheHolding> tranches;
  private Boolean ntFlag;
  private Boolean niFlag;
  private String comment;
  private Date createDate;
  private Date updateDate;

  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public String getHoldingId() {
    return holdingId;
  }

  public void setHoldingId(String holdingId) {
    this.holdingId = holdingId;
  }

  public String getClient() {
    return client;
  }

  public void setClient(String client) {
    this.client = client;
  }

  public String getDeal() {
    return deal;
  }

  public void setDeal(String deal) {
    this.deal = deal;
  }

  public List<TrancheHolding> getTranches() {
    return tranches;
  }

  public void setTranches(List<TrancheHolding> tranches) {
    this.tranches = tranches;
  }

  public Boolean getNtFlag() {
    return ntFlag;
  }

  public void setNtFlag(Boolean ntFlag) {
    this.ntFlag = ntFlag;
  }

  public Boolean getNiFlag() {
    return niFlag;
  }

  public void setNiFlag(Boolean niFlag) {
    this.niFlag = niFlag;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  public HoldingType getType() {
    return type;
  }

  public void setType(HoldingType type) {
    this.type = type;
  }

}
