package com.etradingsoftware.levfin.levdemodata.model;



import java.util.Date;
import java.util.UUID;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "axe")
public class Axe {

  @Id
  private ObjectId id;

  private String axeId;
  private String side;
  private Date createDate;
  private Date updateDate;

  private String client;
  private String deal;
  private String tranche;
  private Date rollDate;
  private int size;
  private double price;
  private double indicativePrice;
  private String trader;
  private String tradeStatus;
  private String comments;

  public Axe() {}

  public Axe(String id) {
    this.axeId = id;
  }

  public Axe(String side, String client, String deal, String tranche) {
    this.side = side;
    this.client = client;
    this.deal = deal;
    this.tranche = tranche;
  }

  public static Axe createAxe() {
    Axe newAxe = new Axe();
    newAxe.axeId = UUID.randomUUID().toString();
    return newAxe;
  }

  public String getAxeId() {
    return axeId;
  }

  public void setAxeId(String axeId) {
    this.axeId = axeId;
  }

  public String getSide() {
    return side;
  }

  public void setSide(String side) {
    this.side = side;
  }

  public String getDeal() {
    return deal;
  }

  public void setDeal(String deal) {
    this.deal = deal;
  }

  public String getTranche() {
    return tranche;
  }

  public void setTranche(String tranche) {
    this.tranche = tranche;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public String getTrader() {
    return trader;
  }

  public void setTrader(String trader) {
    this.trader = trader;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  public String getTradeStatus() {
    return tradeStatus;
  }

  public void setTradeStatus(String tradeStatus) {
    this.tradeStatus = tradeStatus;
  }

  public double getIndicativePrice() {
    return indicativePrice;
  }

  public void setIndicativePrice(double indicativePrice) {
    this.indicativePrice = indicativePrice;
  }

  public String getClient() {
    return client;
  }

  public void setClient(String client) {
    this.client = client;
  }

  public Date getRollDate() {
    return rollDate;
  }

  public void setRollDate(Date rollDate) {
    this.rollDate = rollDate;
  }

  @Override
  public String toString() {
    return "Axe [ { id:\"" + id + "\", axeId:\"" + axeId + "\", side:\"" + side
        + "\", createDate:\"" + createDate + "\", updateDate:\"" + updateDate + "\", client:\""
        + client + "\", deal:\"" + deal + "\", tranche:\"" + tranche + "\", rollDate:\"" + rollDate
        + "\", size:\"" + size + "\", price:\"" + price + "\", indicativePrice:\"" + indicativePrice
        + "\", trader:\"" + trader + "\", tradeStatus:\"" + tradeStatus + "\", comments:\""
        + comments + "}]";
  }

}
