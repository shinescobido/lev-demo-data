package com.etradingsoftware.levfin.levdemodata.model;

import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "client")
public class Client {

  @Id
  private ObjectId id;

  private String clientId;
  private String name;
  private String[] alias;
  private List<Trader> traders;

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String[] getAlias() {
    return alias;
  }

  public void setAlias(String[] alias) {
    this.alias = alias;
  }

  public List<Trader> getTraders() {
    return traders;
  }

  public void setTraders(List<Trader> traders) {
    this.traders = traders;
  }

}
