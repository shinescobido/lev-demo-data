package com.etradingsoftware.levfin.levdemodata.model;

public class Tranche {
  
  private String trancheId;
  private String name;
  private String dealId;
  private String bbgID;
  private String lxID;
  
 
  
  public Tranche(String name, String bbgID, String lxID) {
    super();
    this.name = name;
    this.bbgID = bbgID;
    this.lxID = lxID;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBbgID() {
    return bbgID;
  }

  public void setBbgID(String bbgID) {
    this.bbgID = bbgID;
  }

  public String getLxID() {
    return lxID;
  }

  public void setLxID(String lxID) {
    this.lxID = lxID;
  }

  public String getTrancheId() {
    return trancheId;
  }

  public void setTrancheId(String trancheId) {
    this.trancheId = trancheId;
  }

  public String getDealId() {
    return dealId;
  }

  public void setDealId(String dealId) {
    this.dealId = dealId;
  }

}
