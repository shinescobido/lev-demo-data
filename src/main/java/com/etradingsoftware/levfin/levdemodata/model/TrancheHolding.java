package com.etradingsoftware.levfin.levdemodata.model;

public class TrancheHolding {
 
  private String trancheId;
  private Double sum;
  private Boolean flag;
  
  public String getTrancheId() {
    return trancheId;
  }
  public void setTrancheId(String trancheId) {
    this.trancheId = trancheId;
  }
  public Double getSum() {
    return sum;
  }
  public void setSum(Double sum) {
    this.sum = sum;
  }
  public Boolean getFlag() {
    return flag;
  }
  public void setFlag(Boolean flag) {
    this.flag = flag;
  }
}
