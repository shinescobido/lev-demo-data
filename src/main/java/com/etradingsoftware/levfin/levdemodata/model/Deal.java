package com.etradingsoftware.levfin.levdemodata.model;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "deal")
public class Deal {

  @Id
  private ObjectId id;
  
  private String dealId;
  private String name;
  private String[] alias;
  private List<Tranche> tranches;
  private String trader;
  private String agent;
  private String fees;
  private String docs;
  private Date createDate;
  private Date updateDate;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String[] getAlias() {
    return alias;
  }

  public void setAlias(String[] alias) {
    this.alias = alias;
  }

  public List<Tranche> getTranches() {
    return tranches;
  }

  public void setTranches(List<Tranche> tranches) {
    this.tranches = tranches;
  }

  public String getTrader() {
    return trader;
  }

  public void setTrader(String trader) {
    this.trader = trader;
  }

  public String getAgent() {
    return agent;
  }

  public void setAgent(String agent) {
    this.agent = agent;
  }

  public String getFees() {
    return fees;
  }

  public void setFees(String fees) {
    this.fees = fees;
  }

  public String getDocs() {
    return docs;
  }

  public void setDocs(String docs) {
    this.docs = docs;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((agent == null) ? 0 : agent.hashCode());
    result = prime * result + Arrays.hashCode(alias);
    result = prime * result + ((docs == null) ? 0 : docs.hashCode());
    result = prime * result + ((fees == null) ? 0 : fees.hashCode());
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + ((trader == null) ? 0 : trader.hashCode());
    result = prime * result + ((tranches == null) ? 0 : tranches.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Deal other = (Deal) obj;
    if (agent == null) {
      if (other.agent != null)
        return false;
    } else if (!agent.equals(other.agent))
      return false;
    if (!Arrays.equals(alias, other.alias))
      return false;
    if (docs == null) {
      if (other.docs != null)
        return false;
    } else if (!docs.equals(other.docs))
      return false;
    if (fees == null) {
      if (other.fees != null)
        return false;
    } else if (!fees.equals(other.fees))
      return false;
    if (name == null) {
      if (other.name != null)
        return false;
    } else if (!name.equals(other.name))
      return false;
    if (trader == null) {
      if (other.trader != null)
        return false;
    } else if (!trader.equals(other.trader))
      return false;
    if (tranches == null) {
      if (other.tranches != null)
        return false;
    } else if (!tranches.equals(other.tranches))
      return false;
    return true;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  public String getDealId() {
    return dealId;
  }

  public void setDealId(String dealId) {
    this.dealId = dealId;
  }

}
