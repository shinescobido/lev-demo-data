package com.etradingsoftware.levfin.levdemodata.model;

import java.util.Date;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
@Document(collection = "trade")
public class Trade {

  @Id
  private ObjectId id;

  private String tradeId;
  private String side;
  private Date createDate;
  private Date updateDate;
  private String deal;
  private String client;
  private String tranche;
  private int size;
  private double price;
  private String terms;
  private String trader;
  private String fees;
  private String comments;
  private String ex;
  private String sales;
  
  public Trade() {
  }

  public Trade(String tradeId) {
    this.tradeId = tradeId;
  }

  public String getTradeId() {
    return tradeId;
  }

  public void setTradeId(String tradeId) {
    this.tradeId = tradeId;
  }

  public String getSide() {
    return side;
  }

  public void setSide(String side) {
    this.side = side;
  }


  public String getDeal() {
    return deal;
  }

  public void setDeal(String deal) {
    this.deal = deal;
  }

  public String getTranche() {
    return tranche;
  }

  public void setTranche(String tranche) {
    this.tranche = tranche;
  }

  public int getSize() {
    return size;
  }


  public void setSize(int size) {
    this.size = size;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public String getTrader() {
    return trader;
  }

  public void setTrader(String trader) {
    this.trader = trader;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getTerms() {
    return terms;
  }

  public void setTerms(String terms) {
    this.terms = terms;
  }

  public String getFees() {
    return fees;
  }

  public void setFees(String fees) {
    this.fees = fees;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  public String getEx() {
    return ex;
  }

  public void setEx(String ex) {
    this.ex = ex;
  }

  public String getSales() {
    return sales;
  }

  public void setSales(String sales) {
    this.sales = sales;
  }

  public String getClient() {
    return client;
  }

  public void setClient(String client) {
    this.client = client;
  }

}
