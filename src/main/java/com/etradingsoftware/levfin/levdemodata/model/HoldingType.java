package com.etradingsoftware.levfin.levdemodata.model;

public enum HoldingType {
  AXE,
  TRADE
}
