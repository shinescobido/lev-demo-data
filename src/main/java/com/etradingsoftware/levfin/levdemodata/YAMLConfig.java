package com.etradingsoftware.levfin.levdemodata;

import java.util.HashSet;
import java.util.Set;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import com.etradingsoftware.levfin.levdemodata.model.Client;
import com.etradingsoftware.levfin.levdemodata.model.Deal;


@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
public class YAMLConfig {
  
  private Set<String> traders = new HashSet<String>();
  private Set<Client> clients = new HashSet<Client>();
  private Set<Deal> deals = new HashSet<Deal>();
  private int noOfDays;

  public Set<String> getTraders() {
    return traders;
  }

  public void setTraders(Set<String> traders) {
    this.traders = traders;
  }

  public Set<Client> getClients() {
    return clients;
  }

  public void setClients(Set<Client> clients) {
    this.clients = clients;
  }

  public Set<Deal> getDeals() {
    return deals;
  }

  public void setDeals(Set<Deal> deals) {
    this.deals = deals;
  }

  public int getNoOfDays() {
    return noOfDays;
  }

  public void setNoOfDays(int noOfDays) {
    this.noOfDays = noOfDays;
  }

}
