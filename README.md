# LevFin Demo Data #

A not-so-slick demo data generator ;)

### What is this repository for? ###

* Generate data for Deal, Tranche, Axe, Trade and Client collections 

### How do I get set up? ###

* How to run
```python
mvn spring-boot:run [or]
java -jar lev-demo-data.jar (compiled)
```
* Configuration
update database config in application.yaml file
```python
spring:
   data:
      mongodb:
         authentication-database: admin
         username: admin1
         password: password1
         database: LEVFIN_DEMO
         port: 27017
         host: localhost
```

